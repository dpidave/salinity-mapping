http://data.environment.nsw.gov.au/dataset/monitoring-evaluation-and-reporting-salinity-pressure-and-land-management-summary-report-carfe37c

Monitoring, Evaluation and Reporting Salinity Pressure and Land Management Summary - Report Card Scores and Trends (5 December 2008)
This dataset contains a summary of the derived salinity condition scores generated for the Soil Condition and Land Mangaement within Capability themes of the NSW Monitoring, Evaluation & Reporting (MER) program. It also gives details of scores, trends, data sources and confidence for salinity pressure and land management of salinised land within its capability.

The salinity condition scores were generated from 'known saline site' mapping and API activities carried out by DLWC and DIPNR regions between 2000 and 2005. Scores are based on location, size and intensity of salinised areas and their impacts on in-stream salt load and electrical conductivity. The overall salinity condition score ranges from 5 (minimal salinity impact on land and water) through to 1 (severe salinity impact on land and water).

Expert opinion from staff working in the salinity field was used to assess salinity pressure and land management scores and trends for each SMU being considered. Salinity pressure and land management scores are rated from 5 (minimal) through to 1 (severe).

MER Salinity Pressure and Land Management Summary - Report Card Scores and Trends (5 December 2008) XLS
A summary of the derived salinity condition scores generated for the Soil...

This data was generated for input into the reporting process for the Soil Condition and Land Management within Capability themes of the NSW Monitoring, Evaluation & Reporting (MER) program. The dataset does not represent all the information available on salinity extent or impact. The scale being used is very broad and at best gives a catchment scale picture at a point in time. SMUs do not cover entire CMA areas. Salinity data has only been considered for the SMU as defined by the Soil Condition theme. There are salt affected areas outside the current SMUs. Users of this dataset should be aware that the salinity outbreak datasets used for this evaluation are of differing ages and of varying quality.

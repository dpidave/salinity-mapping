## README.md for mapping project using basemap
Overall aim: To plot some salinity data onto our own map  

### Taking a look at open data  
Section aim:  To look at open data available via the govt and to think about
formats  

1.  Look at open data on NSW govt (https://data.nsw.gov.au/blog)  
2.  See how data formats can matter
    (https://data.nsw.gov.au/data/dataset?q=soil&sort=score+desc%2C+metadata_modified+desc
)  
3.  See the OEH data portal and the search options (soil, climate, water etc)  
4.  See the data viewers espade
    (http://www.environment.nsw.gov.au/topics/land-and-soil/soil-data)  
5.  Find some actual data on salinity
    (http://data.environment.nsw.gov.au/dataset/monitoring-evaluation-and-reporting-soil-condition-salinity-report-card-score-calculations-1e742/resource/654528ef-f9c6-468f-8ea9-efe29bf2bcc7)  
6.  The actual spreadsheet is ```Salinityscores090218.xls```  

### Getting the data ready for mapping  
Section aim: cleanup the spreadsheet to only include data we are interested in
plotting, as well as adding data to make the plots work  

1.  Look up the wikipage on long and lat cordinates  
2.  Use google and a script to add town coordinates for our table, see
    ```Australian_Post_Codes_Lat_lon.zip```  
3.  Use google maps to get the coordinates of Orange NSW  
4.  Use google maps to get the coordinates of NSW  

### Thinking about mapping this data onto the state map  
Section aim: Use the basemap package to draw some simple maps  

1.  Go through the notebook and see how things work by having a play  
2.  Try to make your own maps with specific data from the spreadsheet   

### Final  
-  Hopefully you have had a taste of the value of data  
-  That programers are lazy!  
